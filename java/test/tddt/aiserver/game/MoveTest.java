package tddt.aiserver.game;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class MoveTest
{
	@Test
	public void step1HasTheValueSetInTheConstructor() throws Exception
	{
		Direction step1 = Direction.DOWN;
		Move move = new Move(step1);

		assertThat(move.step1, is(step1));
	}

	@Test
	public void step2HasTheValueSetInTheConstructor() throws Exception
	{
		Direction step2 = Direction.UP;
		Move move = new Move(Direction.LEFT, step2);

		assertThat(move.step2, is(step2));
	}

	@Test(expected = IllegalArgumentException.class)
	public void step1MustNotBeNull() throws Exception
	{
		new Move(null);
	}

	@Test
	public void sameMovesShouldEqual() throws Exception
	{
		assertThat(new Move(Direction.RIGHT), equalTo(new Move(Direction.RIGHT)));
		assertThat(new Move(Direction.LEFT, Direction.UP), equalTo(new Move(Direction.LEFT, Direction.UP)));
	}

	@Test
	public void differentMovesShouldNotEqual() throws Exception
	{
		assertThat(new Move(Direction.RIGHT), not(equalTo(new Move(Direction.LEFT))));
		assertThat(new Move(Direction.RIGHT, Direction.RIGHT), not(equalTo(new Move(Direction.RIGHT))));
		assertThat(new Move(Direction.UP, Direction.DOWN), not(equalTo(new Move(Direction.DOWN, Direction.UP))));
	}
}