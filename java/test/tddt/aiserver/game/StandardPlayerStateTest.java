package tddt.aiserver.game;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class StandardPlayerStateTest
{
	private Point[] SEGMENTS = new Point[] {
			new Point(1, 1),
			new Point(1, 2),
			new Point(2, 2)
	};

	private StandardPlayerState PLAYER_STATE;

	@Before
	public void setUp() throws Exception
	{
		PLAYER_STATE = new StandardPlayerState(SEGMENTS[0]);
		PLAYER_STATE.addSegment(SEGMENTS[1]);
		PLAYER_STATE.addSegment(SEGMENTS[2]);
	}

	@Test
	public void testGetHead() throws Exception
	{
		assertThat(PLAYER_STATE.getHead(), equalTo(SEGMENTS[2]));
	}

	@Test
	public void testGetAllSegments() throws Exception
	{
		assertThat(PLAYER_STATE.getAllSegments(), equalTo(SEGMENTS));
	}

	@Test
	public void testCreateFromOtherPlayerState() throws Exception
	{
		PlayerState playerState = new StandardPlayerState(PLAYER_STATE);
		assertThat(playerState.getHead(), equalTo(PLAYER_STATE.getHead()));
		assertThat(playerState.getAllSegments(), equalTo(PLAYER_STATE.getAllSegments()));
	}
}