package tddt.aiserver.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class StandardGamePlanTest
{
	@Test(expected = IllegalStateException.class)
	public void startingPositionOnWallShouldThrowException() throws Exception
	{
		Point wall = new Point(1, 2);

		new StandardGamePlan(3, 3,
				ImmutableList.of(wall),
				ImmutableSet.of(wall));
	}

	@Test(expected = IllegalStateException.class)
	public void startingPositionOutsideOfThePlanShouldThrowException() throws Exception
	{
		new StandardGamePlan(3, 3,
				ImmutableList.of(new Point(3, 2)));
	}

	@Test
	public void testCreateFromOtherGamePlan() throws Exception
	{
		GamePlan plan1 = new StandardGamePlan(3, 4,
				ImmutableList.of(new Point(1, 1), new Point(2, 2)),
				ImmutableSet.of(new Point(0, 0), new Point(0, 1)));

		GamePlan plan2 = new StandardGamePlan(plan1);

		assertThat(plan1.getWidth(), is(plan2.getWidth()));
		assertThat(plan1.getHeight(), is(plan2.getHeight()));
		assertThat(plan1.getStartingPositions(), equalTo(plan2.getStartingPositions()));
		assertThat(plan1.getWalls(), equalTo(plan2.getWalls()));
	}
}