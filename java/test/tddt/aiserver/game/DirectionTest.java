package tddt.aiserver.game;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class DirectionTest
{
	private static Point FROM = new Point(3, 7);

	@Test
	public void leftReturnsCorrectDestination() throws Exception
	{
		assertThat(Direction.LEFT.from(FROM), equalTo(new Point(2, 7)));
	}

	@Test
	public void rightReturnsCorrectDestination() throws Exception
	{
		assertThat(Direction.RIGHT.from(FROM), equalTo(new Point(4, 7)));
	}

	@Test
	public void upReturnsCorrectDestination() throws Exception
	{
		assertThat(Direction.UP.from(FROM), equalTo(new Point(3, 6)));
	}

	@Test
	public void downReturnsCorrectDestination() throws Exception
	{
		assertThat(Direction.DOWN.from(FROM), equalTo(new Point(3, 8)));
	}
}