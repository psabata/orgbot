package tddt.aiserver.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jpomikalek on 11/26/2015.
 */
public class StandardGameStateTest
{
	public static final Player PLAYER1 = new Player(1, "Player 1");
	public static final Player PLAYER2 = new Player(2, "Player 2");

	public static final Point PLAYER1_START_12_X_12 = new Point(11, 5);
	public static final Point PLAYER2_START_12_X_12 = new Point(11, 3);

	public static final List<Point> STARTING_POSITIONS_12_X_12 = ImmutableList.of(PLAYER1_START_12_X_12, PLAYER2_START_12_X_12);

	public static final Set<Point> WALLS_12_X_12 = ImmutableSet.of(new Point(11, 6));

	public static final GamePlan GAME_PLAN_12_X_12 = new StandardGamePlan(12, 12, STARTING_POSITIONS_12_X_12, WALLS_12_X_12);

	@Test(expected = IllegalStateException.class)
	public void mismatchBetweenStartingPositionsCountAndPlayerCountShouldThrowException() throws Exception
	{
		new StandardGameState(GAME_PLAN_12_X_12, PLAYER1);
	}

	@Test
	public void testGetPlayers() throws Exception
	{
		GameState gameState = new StandardGameState(GAME_PLAN_12_X_12, PLAYER1, PLAYER2);
		assertThat(ImmutableList.copyOf(gameState.getPlayers()), equalTo(ImmutableList.of(PLAYER1, PLAYER2)));
	}

	@Test
	public void isEmptyFieldShouldReturnTrueOnEmptyField() throws Exception
	{
		GameState gameState = new StandardGameState(GAME_PLAN_12_X_12, PLAYER1, PLAYER2);
		assertThat(gameState.isEmptyField(new Point(0, 0)), is(true));
		assertThat(gameState.isEmptyField(2, 1), is(true));
	}

	@Test
	public void isEmptyFieldShouldReturnFalseOnFieldOccupiedByPlayer() throws Exception
	{
		GameState gameState = new StandardGameState(GAME_PLAN_12_X_12, PLAYER1, PLAYER2);
		assertThat(gameState.isEmptyField(PLAYER1_START_12_X_12), is(false));
		assertThat(gameState.isEmptyField(PLAYER2_START_12_X_12.x, PLAYER2_START_12_X_12.y), is(false));
	}

	@Test
	public void isEmptyFieldShouldReturnFalseOnWall() throws Exception
	{
		GameState gameState = new StandardGameState(GAME_PLAN_12_X_12, PLAYER1, PLAYER2);
		assertThat(gameState.isEmptyField(WALLS_12_X_12.iterator().next()), is(false));
	}

	@Test
	public void isEmptyFieldShouldReturnFalseOnPositionOutsideOfGamePlan() throws Exception
	{
		GameState gameState = new StandardGameState(GAME_PLAN_12_X_12, PLAYER1, PLAYER2);
		assertThat(gameState.isEmptyField(-1, 0), is(false));
		assertThat(gameState.isEmptyField(0, GAME_PLAN_12_X_12.getHeight()), is(false));
	}

	@Test
	public void playerStatesShouldBeInitializedFromGamePlan() throws Exception
	{
		StandardGameState gameState = new StandardGameState(GAME_PLAN_12_X_12, PLAYER1, PLAYER2);

		assertThat(gameState.getPlayerState(PLAYER1).getHead(), equalTo(PLAYER1_START_12_X_12));
		assertThat(gameState.getPlayerState(PLAYER2).getHead(), equalTo(PLAYER2_START_12_X_12));
	}

	@Test(expected = IllegalStateException.class)
	public void duplicatePlayerIdShouldThrowException() throws Exception
	{
		new StandardGameState(GAME_PLAN_12_X_12, PLAYER1, new Player(PLAYER1.id, "Duplicate ID player"));
	}
}
