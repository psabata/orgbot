package tddt.aiserver.game;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class PlayerTest
{
	private Player PLAYER1 = new Player(1, "Player 1");
	private Player PLAYER1_COPY = new Player(1, "Player 1 copy");
	private Player PLAYER2 = new Player(2, "Player 2");

	@Test
	public void idHasTheValueSetInTheConstructor() throws Exception
	{
		int id = 123;
		Player player = new Player(id, "whatever");

		assertThat(player.id, is(id));
	}

	@Test
	public void nameHasTheValueSetInTheConstructor() throws Exception
	{
		String name = "Antonin";
		Player player = new Player(11, name);

		assertThat(player.name, is(name));
	}

	@Test
	public void playersWithTheSameIdEqual() throws Exception
	{
		assertThat(PLAYER1, equalTo(PLAYER1));
		assertThat(PLAYER1, equalTo(PLAYER1_COPY));
	}

	@Test
	public void playersWithDifferentIdsDoNotEqual() throws Exception
	{
		assertThat(PLAYER1, not(equalTo(PLAYER2)));
	}

	@Test
	public void playersWhoEqualHaveTheSameHashCode() throws Exception
	{
		List<Player> players = ImmutableList.of(PLAYER1, PLAYER1_COPY, PLAYER2);

		for (Player player1 : players)
		{
			for (Player player2 : players)
			{
				if (player1.equals(player2))
					assertThat(player1 + " and " + player2 + " should have the same hashCode", player1.hashCode(), is(player2.hashCode()));
			}
		}
	}
}