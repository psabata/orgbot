package tddt.aiserver.game;

import com.google.common.collect.ImmutableSet;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Created by jpomikalek on 12/2/2015.
 */
public class GameStateCreatorTest
{
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void testCreatingFromValidString() throws Exception
	{
		String gameStateAsString = "*1 \n" +
								   "2 *\n";
		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(gameState.getGamePlan().getWidth(), is(3));
		assertThat(gameState.getGamePlan().getHeight(), is(2));

		Set<Point> expectedWalls = ImmutableSet.of(new Point(0, 0), new Point(2, 1));
		assertThat(gameState.getGamePlan().getWalls(), equalTo(expectedWalls));

		assertTrue(gameState.isEmptyField(2, 0));
		assertTrue(gameState.isEmptyField(1, 1));

		assertThat(gameState.getPlayers().size(), is(2));

		assertThat(gameState.getPlayerState(1).getHead(), equalTo(new Point(1, 0)));
		assertThat(gameState.getPlayerState(2).getHead(), equalTo(new Point(0, 1)));
	}

	@Test
	public void invalidCharacterShouldThrowException() throws Exception
	{
		expectedException.expect(GameStateCreator.GameStateCreationException.class);
		expectedException.expectMessage("unrecognized character: #");

		GameStateCreator.fromString("**1#");
	}

	@Test
	public void nonRectangularPlanShouldThrowException() throws Exception
	{
		expectedException.expect(GameStateCreator.GameStateCreationException.class);
		expectedException.expectMessage("non-rectangular plan: line 3 width (3) is different from the line 1 width (4)");

		GameStateCreator.fromString("****\n****\n***\n****");
	}
}
