package tddt.aiserver.game;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SampleAiTest
{
	@Test
	public void testAvoidsObstacles() throws Exception
	{
		String gameStateAsString = "***\n" +
								   "*1*\n" +
								   "   \n";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(new SampleAi().makeMove(1, gameState).step1, is(Direction.DOWN));
	}

	@Test
	public void testAvoidsObstacles2() throws Exception
	{
		String gameStateAsString = "* **\n" +
								   "*12 \n" +
								   " 34*\n" +
								   "** *\n";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(new SampleAi().makeMove(1, gameState), equalTo(Move.UP_));
		assertThat(new SampleAi().makeMove(2, gameState), equalTo(Move.RIGHT_));
		assertThat(new SampleAi().makeMove(3, gameState), equalTo(Move.LEFT_));
		assertThat(new SampleAi().makeMove(4, gameState), equalTo(Move.DOWN_));
	}
}