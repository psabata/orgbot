package tddt.aiserver.gameutil;

import org.junit.Test;
import tddt.aiserver.game.GameState;
import tddt.aiserver.game.GameStateCreator;
import tddt.aiserver.game.Point;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GameWalkerTest {

    @Test
    public void testSizeOfRegion() throws Exception {
        String gameStateAsString =
                "    \n" +
                "    \n" +
                "   3\n" +
                "    \n";

        GameState gameState = GameStateCreator.fromString(gameStateAsString);

        assertThat(GameWalker.regionSize(gameState, new Point(0, 0)), is(15));
    }

    @Test
    public void testSizeOfEnclosedRegion() throws Exception {
        String gameStateAsString =
                "     \n" +
                "*****\n" +
                "*   *\n" +
                "*   *\n" +
                "**  *\n" +
                "*   *\n" +
                "*****\n";

        GameState gameState = GameStateCreator.fromString(gameStateAsString);

        assertThat(GameWalker.regionSize(gameState, new Point(3, 3)), is(11));
        assertThat(GameWalker.regionSize(gameState, new Point(3, 0)), is(5));
    }

    @Test
    public void testSizeOfWarpedRegion() throws Exception {
        String gameStateAsString =
                "     \n" +
                "*****\n" +
                "*   *\n" +
                " *   \n" +
                "**  *\n" +
                "* ***\n" +
                "**  *\n" +
                "     \n";

        GameState gameState = GameStateCreator.fromString(gameStateAsString);

        assertThat(GameWalker.regionSize(gameState, new Point(3, 3)), is(9));
        assertThat(GameWalker.regionSize(gameState, new Point(0, 3)), is(9));
        assertThat(GameWalker.regionSize(gameState, new Point(2, 0)), is(12));
        assertThat(GameWalker.regionSize(gameState, new Point(3, 6)), is(12));
    }

    @Test
    public void testInvalidRegion() throws Exception {
        String gameStateAsString =
                "     \n" +
                "*****\n" +
                "*   *\n" +
                " *   \n" +
                "**  *\n" +
                "* ***\n" +
                "**  *\n" +
                "     \n";

        GameState gameState = GameStateCreator.fromString(gameStateAsString);

        assertThat(GameWalker.regionSize(gameState, new Point(2, 1)), is(0));
        assertThat(GameWalker.regionSize(gameState, new Point(5, 3)), is(0));
    }

    @Test
    public void testCountBotsInEnclosedRegion() throws Exception {
        String gameStateAsString =
                "     \n" +
                "**1**\n" +
                "*   *\n" +
                "*4  *\n" +
                "**  *\n" +
                "* 3 *\n" +
                "*****\n";

        GameState gameState = GameStateCreator.fromString(gameStateAsString);

        assertThat(GameWalker.countBotsInRegion(gameState, new Point(2, 0), 1), is(0));
        assertThat(GameWalker.countBotsInRegion(gameState, new Point(2, 2), 1), is(2));
    }

    @Test
    public void testCountBotsInEnclosedRegion2() throws Exception {
        String gameStateAsString =
                "     \n" +
                "**1**\n" +
                "*   *\n" +
                "*4  *\n" +
                "**  *\n" +
                "**3 *\n" +
                "*2***\n";

        GameState gameState = GameStateCreator.fromString(gameStateAsString);

        assertThat(GameWalker.countBotsInRegion(gameState, new Point(2, 0), 1), is(1));
        assertThat(GameWalker.countBotsInRegion(gameState, new Point(2, 2), 1), is(2));
    }

}