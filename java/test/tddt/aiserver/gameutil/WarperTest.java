package tddt.aiserver.gameutil;

import org.junit.Test;
import tddt.aiserver.game.Direction;
import tddt.aiserver.game.GameState;
import tddt.aiserver.game.GameStateCreator;
import tddt.aiserver.game.Point;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class WarperTest {

    @Test
    public void testMoveToDirection() throws Exception {

        String gameStateAsString =
                "    \n" +
                "    \n" +
                "    \n" +
                "    \n";

        GameState gameState = GameStateCreator.fromString(gameStateAsString);

        assertThat(Warper.moveToDirection(gameState, new Point(2, 2), Direction.UP), is(new Point(2, 1)));
        assertThat(Warper.moveToDirection(gameState, new Point(2, 2), Direction.LEFT), is(new Point(1, 2)));
        assertThat(Warper.moveToDirection(gameState, new Point(2, 2), Direction.DOWN), is(new Point(2, 3)));
        assertThat(Warper.moveToDirection(gameState, new Point(2, 2), Direction.RIGHT), is(new Point(3, 2)));

        assertThat(Warper.moveToDirection(gameState, new Point(0, 2), Direction.LEFT), is(new Point(3, 2)));
        assertThat(Warper.moveToDirection(gameState, new Point(3, 2), Direction.RIGHT), is(new Point(0, 2)));
        assertThat(Warper.moveToDirection(gameState, new Point(1, 0), Direction.UP), is(new Point(1, 3)));
        assertThat(Warper.moveToDirection(gameState, new Point(3, 3), Direction.DOWN), is(new Point(3, 0)));
    }

}