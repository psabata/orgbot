package tddt.aiserver.sockets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.*;

/**
 * Created by jpomikalek on 11/13/2015.
 */
public class SimpleServerTest
{
	private int PORT = 4444;
	private Thread SERVER_THREAD;

	@Before
	public void setUp() throws Exception
	{
		startServer();
	}

	@After
	public void tearDown() throws Exception
	{
		if (SERVER_THREAD.isAlive())
			shutdownServer();
	}

	@Test
	public void testShuttingDown() throws Exception
	{
		assertTrue(SERVER_THREAD.isAlive());
		shutdownServer();
		Thread.sleep(100);
		assertFalse(SERVER_THREAD.isAlive());
	}

	@Test
	public void testUptimeRequest() throws Exception
	{
		Thread.sleep(1500);
		int upTime = Integer.valueOf(requestServerResponse(SimpleServer.UPTIME_REQUEST));
		assertThat(upTime, greaterThan(0));
	}

	@Test
	public void testRequestResponse() throws Exception
	{
		assertThat(requestServerResponse("FooBar"), is("foobar"));
		assertThat(requestServerResponse("NextREQUEST"), is("nextrequest"));
	}

	private void startServer()
	{
		SERVER_THREAD = new Thread(new SimpleServer(PORT, new ToLowerCaseConverter()));
		SERVER_THREAD.start();
	}

	private void shutdownServer() throws IOException
	{
		requestServerResponse(SimpleServer.SHUTDOWN_REQUEST);
	}

	private String requestServerResponse(String request) throws IOException
	{
		Socket socket = new Socket("localhost", PORT);
		PrintWriter inputReader = new PrintWriter(socket.getOutputStream(), true);
		BufferedReader outputWriter = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		inputReader.println(request);
		String response = outputWriter.readLine();
		socket.close();
		return response;
	}

	private static class ToLowerCaseConverter implements SimpleRequestHandler
	{
		@Override
		public String processRequest(String request)
		{
			return request.toLowerCase();
		}
	}
}
