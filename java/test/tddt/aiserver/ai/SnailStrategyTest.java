package tddt.aiserver.ai;

import org.junit.Test;
import tddt.aiserver.game.GameState;
import tddt.aiserver.game.GameStateCreator;
import tddt.aiserver.game.Move;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SnailStrategyTest {

	@Test
	public void firstMoveIsLeft() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.UP_);

		String gameStateAsString = "" +
				"   \n" +
				" 1 \n" +
				"   ";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		Move move = snailStrategy.makeMove(1, gameState);

		assertThat(move, is(Move.LEFT_));
	}

	@Test
	public void whenPreviousMoveIsLeft_nextMoveIsDown() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.LEFT_);

		String gameStateAsString = "" +
				"   \n" +
				" 1 \n" +
				"   ";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		Move move = snailStrategy.makeMove(1, gameState);

		assertThat(move, is(Move.DOWN_));
	}

	@Test
	public void whenWallLeft_nextMoveIsUp() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.UP_);

		String gameStateAsString = "" +
				"   \n" +
				"*1 \n" +
				"   ";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		Move move = snailStrategy.makeMove(1, gameState);

		assertThat(move, is(Move.UP_));
	}

	@Test
	public void whenInCorner_goRightAndUp() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.UP_);

		String gameStateAsString1 = "" +
				" * \n" +
				"*1 \n" +
				"   ";

		GameState gameState1 = GameStateCreator.fromString(gameStateAsString1);

		Move firstMove = snailStrategy.makeMove(1, gameState1);
		assertThat(firstMove, is(Move.RIGHT_));

		String gameStateAsString2 = "" +
				" * \n" +
				"**1\n" +
				"   ";

		GameState gameState2 = GameStateCreator.fromString(gameStateAsString2);

		Move secondMove = snailStrategy.makeMove(1, gameState2);
		assertThat(secondMove, is(Move.UP_));
	}

	@Test
	public void whenNextToPlanBorder_Warp() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.UP_);

		String gameStateAsString1 = "" +
				"1  \n" +
				"   \n" +
				"   ";

		GameState gameState1 = GameStateCreator.fromString(gameStateAsString1);

		Move firstMove = snailStrategy.makeMove(1, gameState1);
		assertThat(firstMove, is(Move.LEFT_));
	}

	@Test
	public void whenInDangerOfCrashingToAnotherPlayer_selectDifferentMove() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.RIGHT_);

		String gameStateAsString1 = "" +
				"     \n" +
				"  2  \n" +
				"     \n" +
				" *1  \n" +
				"     ";

		GameState gameState = GameStateCreator.fromString(gameStateAsString1);

		Move firstMove = snailStrategy.makeMove(1, gameState);
		assertThat(firstMove, is(Move.RIGHT_));
	}

	@Test
	public void whenDangerIsTheOnlyOption_goThere() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.RIGHT_);

		String gameStateAsString1 = "" +
				"     \n" +
				"  2  \n" +
				"     \n" +
				" *1* \n" +
				"  *  ";

		GameState gameState = GameStateCreator.fromString(gameStateAsString1);

		Move firstMove = snailStrategy.makeMove(1, gameState);
		assertThat(firstMove, is(Move.UP_));
	}

	@Test
	public void whenDangerOnTwoPaths_chooseTheThird() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.RIGHT_);

		String gameStateAsString1 = "" +
				"    \n" +
				"  2 \n" +
				"    \n" +
				"3*1 \n" +
				"    ";

		GameState gameState = GameStateCreator.fromString(gameStateAsString1);

		Move firstMove = snailStrategy.makeMove(1, gameState);
		assertThat(firstMove, is(Move.DOWN_));
	}

	@Test
	public void whenOneOptionIsSmaller_chooseBigger() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.RIGHT_);

		String gameStateAsString1 = "" +
				"    \n" +
				"  * \n" +
				"** *\n" +
				" *1*\n" +
				"    ";

		GameState gameState = GameStateCreator.fromString(gameStateAsString1);

		Move firstMove = snailStrategy.makeMove(1, gameState);
		assertThat(firstMove, is(Move.DOWN_));
	}

	@Test
	public void whenSafeRegionIsTooSmall_chooseBigger() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.RIGHT_);

		String gameStateAsString1 = "" +
				"    \n" +
				"    \n" +
				"    \n" +
				"    \n" +
				"    \n" +
				"    \n" +
				"  2 \n" +
				"*  *\n" +
				" *1*\n" +
				"    \n" +
				"****";

		GameState gameState = GameStateCreator.fromString(gameStateAsString1);

		Move firstMove = snailStrategy.makeMove(1, gameState);
		assertThat(firstMove, is(Move.UP_));
	}

	@Test
	public void whenBigRegionHasTooManyRobots_chooseSmallerOne() {
		SnailStrategy snailStrategy = new SnailStrategy(Move.RIGHT_);

		String gameStateAsString1 = "" +
				"    \n" +
				"  2 \n" +
				"  3 \n" +
				"    \n" +
				"*  *\n" +
				" *1*\n" +
				"    \n" +
				"    \n" +
				"****";

		GameState gameState = GameStateCreator.fromString(gameStateAsString1);

		Move firstMove = snailStrategy.makeMove(1, gameState);
		assertThat(firstMove, is(Move.DOWN_));
	}

}