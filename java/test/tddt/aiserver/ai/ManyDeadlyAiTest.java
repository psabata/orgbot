package tddt.aiserver.ai;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import tddt.aiserver.game.Direction;
import tddt.aiserver.game.GameState;
import tddt.aiserver.game.Move;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class ManyDeadlyAiTest
{
    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void makeSmartMove_exceptionIsThrown_makeRandomMoveIsUsed()
    {
        int aiPlayerId = 0;
        GameState gameState = Mockito.mock(GameState.class);

		Move moveToReturn = Mockito.mock(Move.class);

		MoveStrategy failingStrategy = Mockito.mock(MoveStrategy.class);
		Mockito.when(failingStrategy.makeMove(aiPlayerId, gameState)).thenThrow(new RuntimeException("This should never happen!"));

		MoveStrategy fallbackStrategy = Mockito.mock(MoveStrategy.class);
		Mockito.when(fallbackStrategy.makeMove(aiPlayerId, gameState)).thenReturn(moveToReturn);

        ManyDeadlyAi manyDeadlyAi = new ManyDeadlyAi(failingStrategy, fallbackStrategy);

        assertThat(manyDeadlyAi.makeMove(aiPlayerId, gameState), is(moveToReturn));
    }

    @Test
    public void makeMove_takesMoreThan150ms_exceptionIsThrown()
    {
		int aiPlayerId = 0;
		GameState gameState = Mockito.mock(GameState.class);

		MoveStrategy longRunningStrategy = new MoveStrategy() {
			@Override
			public Move makeMove(int aiPlayerId, GameState gameState) {
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					fail("Failed during sleep!");
				}

				return new Move(Direction.DOWN);
			}
		};

		MoveStrategy fallbackStrategy = Mockito.mock(MoveStrategy.class);

		ManyDeadlyAi manyDeadlyAi = new ManyDeadlyAi(longRunningStrategy, fallbackStrategy);

		thrown.expect(RuntimeException.class);
        manyDeadlyAi.makeMove(aiPlayerId, gameState);
    }

}