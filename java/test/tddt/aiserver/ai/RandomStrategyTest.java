package tddt.aiserver.ai;

import org.junit.Test;
import tddt.aiserver.game.*;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RandomStrategyTest
{
	@Test
	public void testAvoidsObstacles() throws Exception
	{
		String gameStateAsString = "***\n" +
								   "*1*\n" +
								   "   \n";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(new RandomStrategy().makeMove(1, gameState).step1, is(Direction.DOWN));
	}

	@Test
	public void testAvoidsObstacles2() throws Exception
	{
		String gameStateAsString = "* **\n" +
								   "*12 \n" +
								   " 34*\n" +
								   "** *\n";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(new RandomStrategy().makeMove(1, gameState), equalTo(Move.UP_));
		assertThat(new RandomStrategy().makeMove(2, gameState), equalTo(Move.RIGHT_));
		assertThat(new RandomStrategy().makeMove(3, gameState), equalTo(Move.LEFT_));
		assertThat(new RandomStrategy().makeMove(4, gameState), equalTo(Move.DOWN_));
	}

	@Test
	public void testAvoidsObstaclesAndWarping() throws Exception
	{
		String gameStateAsString =
				"*1*\n" +
				"***\n" +
				"   \n";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(new RandomStrategy().makeMove(1, gameState).step1, is(Direction.UP));
	}

	@Test
	public void testAvoidsObstaclesAndWarping2() throws Exception
	{
		String gameStateAsString =
				" **\n" +
				" *1\n" +
				" **\n";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(new RandomStrategy().makeMove(1, gameState).step1, is(Direction.RIGHT));
	}

	@Test
	public void testMaxRegionMoveSimple() throws Exception
	{
		String gameStateAsString =
				"******\n" +
				"* 1  *\n" +
				"******\n";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(new RandomStrategy().makeMove(1, gameState).step1, is(Direction.RIGHT));
		assertThat(new RandomStrategy().makeMove(1, gameState).step1, is(Direction.RIGHT));
		assertThat(new RandomStrategy().makeMove(1, gameState).step1, is(Direction.RIGHT));
	}

	@Test
	public void testMaxRegionMoveComplex() throws Exception
	{
		String gameStateAsString =
				"**** *\n" +
				"**   *\n" +
				"** ***\n" +
				"* 1  *\n" +
				"** ***\n";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(new RandomStrategy().makeMove(1, gameState).step1, is(Direction.UP));
		assertThat(new RandomStrategy().makeMove(1, gameState).step1, is(Direction.UP));
		assertThat(new RandomStrategy().makeMove(1, gameState).step1, is(Direction.UP));
	}

	@Test
	@SuppressWarnings({"unchecked"})
	public void testMaxRegionMoveComplex2() throws Exception
	{
		String gameStateAsString =
				"**** *\n" +
				"**   *\n" +
				"** * *\n" +
				"* 1  *\n" +
				"** ***\n";

		GameState gameState = GameStateCreator.fromString(gameStateAsString);

		assertThat(new RandomStrategy().makeMove(1, gameState).step1, anyOf(is(Direction.UP), is(Direction.RIGHT)));
		assertThat(new RandomStrategy().makeMove(1, gameState).step1, anyOf(is(Direction.UP), is(Direction.RIGHT)));
		assertThat(new RandomStrategy().makeMove(1, gameState).step1, anyOf(is(Direction.UP), is(Direction.RIGHT)));
		assertThat(new RandomStrategy().makeMove(1, gameState).step1, anyOf(is(Direction.UP), is(Direction.RIGHT)));
		assertThat(new RandomStrategy().makeMove(1, gameState).step1, anyOf(is(Direction.UP), is(Direction.RIGHT)));
	}

}