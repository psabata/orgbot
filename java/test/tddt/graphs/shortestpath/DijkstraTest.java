package tddt.graphs.shortestpath;

import com.google.common.collect.Sets;
import org.junit.Test;
import tddt.graphs.Decider;
import tddt.graphs.DirectedGraph;
import tddt.graphs.Edge;
import tddt.graphs.EdgeProvider;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DijkstraTest {

	@Test
	@SuppressWarnings({"unchecked"})
	public void testTrivialGraph() {
		Set<Integer> vertices = Sets.newHashSet(1, 2);

		EdgeProvider<Integer> edgeProvider = new EdgeProvider<Integer>() {
			@Override
			public Set<Edge<Integer>> getEdges(Integer vertex) {
				switch (vertex) {
					case 1 :
						Edge<Integer> edge1 = new Edge<Integer>(1, 2, 2);
						return Sets.newHashSet(edge1);
					case 2 :
						Edge<Integer> edge2 = new Edge<Integer>(2, 1, 1);
						return Sets.newHashSet(edge2);
				}

				return Sets.newHashSet();
			}
		};

		Decider<Integer> decider = new Decider<Integer>() {
			@Override
			public boolean isFinal(Integer v) {
				return v.equals(2);
			}
		};

		ResultPath<Integer> shortestPath = Dijkstra.getInstance(new DirectedGraph<Integer>(vertices, edgeProvider), 1).findShortestPath(decider);
		assertThat(shortestPath.getPath(), is(Arrays.asList(1, 2)));
		assertThat(shortestPath.getLength(), is(2));
	}

	@Test
	@SuppressWarnings({"unchecked"})
	public void testUnreachable() {
		Set<Integer> vertices = Sets.newHashSet(1, 2, 3);

		EdgeProvider<Integer> edgeProvider = new EdgeProvider<Integer>() {
			@Override
			public Set<Edge<Integer>> getEdges(Integer vertex) {
				switch (vertex) {
					case 1 :
						Edge<Integer> edge1 = new Edge<Integer>(1, 2, 2);
						return Sets.newHashSet(edge1);
					case 2 :
						Edge<Integer> edge2 = new Edge<Integer>(2, 1, 2);
						return Sets.newHashSet(edge2);
					case 3 :
						Edge<Integer> edge3 = new Edge<Integer>(3, 1, 2);
						return Sets.newHashSet(edge3);
				}

				return Sets.newHashSet();
			}
		};

		Decider<Integer> decider = new Decider<Integer>() {
			@Override
			public boolean isFinal(Integer v) {
				return v.equals(3);
			}
		};

		ResultPath<Integer> shortestPath = Dijkstra.getInstance(new DirectedGraph<Integer>(vertices, edgeProvider), 1).findShortestPath(decider);
		assertThat(shortestPath.getPath(), is(Collections.<Integer>emptyList()));
		assertThat(shortestPath.getLength(), is(0));
	}

	@Test
	@SuppressWarnings({"unchecked"})
	public void testSimpleGraph() {
		Set<Integer> vertices = Sets.newHashSet(1, 2, 3);

		EdgeProvider<Integer> edgeProvider = new EdgeProvider<Integer>() {
			@Override
			public Set<Edge<Integer>> getEdges(Integer vertex) {
				switch (vertex) {
					case 1 :
						Edge<Integer> edge1 = new Edge<Integer>(1, 2, 2);
						Edge<Integer> edge2 = new Edge<Integer>(1, 3, 5);
						return Sets.newHashSet(edge1, edge2);
					case 2 :
						Edge<Integer> edge3 = new Edge<Integer>(2, 3, 2);
						return Sets.newHashSet(edge3);
					case 3 :
						Edge<Integer> edge4 = new Edge<Integer>(3, 1, 2);
						return Sets.newHashSet(edge4);
				}

				return Sets.newHashSet();
			}
		};

		Decider<Integer> decider = new Decider<Integer>() {
			@Override
			public boolean isFinal(Integer v) {
				return v.equals(3);
			}
		};

		ResultPath<Integer> shortestPath = Dijkstra.getInstance(new DirectedGraph<Integer>(vertices, edgeProvider), 1).findShortestPath(decider);
		assertThat(shortestPath.getPath(), is(Arrays.asList(1, 2, 3)));
		assertThat(shortestPath.getLength(), is(4));
	}

}
