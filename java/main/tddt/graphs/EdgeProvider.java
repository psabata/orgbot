package tddt.graphs;

import java.util.Set;

public interface EdgeProvider<Vertex> {

	Set<Edge<Vertex>> getEdges(Vertex vertex);

}
