package tddt.graphs;

public interface Decider<Vertex> {

	boolean isFinal(Vertex v);

}
