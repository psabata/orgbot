package tddt.graphs;

import java.util.Set;

public class DirectedGraph<Vertex> {

	private final Set<Vertex> vertices;
	private final EdgeProvider<Vertex> edgeProvider;

	public DirectedGraph(Set<Vertex> vertices, EdgeProvider<Vertex> edgeProvider) {
		this.vertices = vertices;
		this.edgeProvider = edgeProvider;
	}

	public boolean contains(Vertex vertex) {
		return vertices.contains(vertex);
	}

	public Set<Edge<Vertex>> getEdges(Vertex vertex) {
		return edgeProvider.getEdges(vertex);
	}

}
