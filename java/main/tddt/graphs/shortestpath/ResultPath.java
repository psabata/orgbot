package tddt.graphs.shortestpath;

import java.util.List;

public class ResultPath<Vertex> {

	private final List<Vertex> path;
	private final int length;

	public ResultPath(List<Vertex> path, int length) {
		this.path = path;
		this.length = length;
	}

	public List<Vertex> getPath() {
		return path;
	}

	public int getLength() {
		return length;
	}

}
