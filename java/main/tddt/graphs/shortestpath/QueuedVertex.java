package tddt.graphs.shortestpath;

import tddt.graphs.Edge;

class QueuedVertex<Vertex> implements Comparable<QueuedVertex<Vertex>> {

	private final Vertex vertex;
	private final Edge<Vertex> edge;
	private final int distance;

	QueuedVertex(Vertex vertex, Edge<Vertex> edge, int distance) {
		this.vertex = vertex;
		this.edge = edge;
		this.distance = distance;
	}

	public Vertex getVertex() {
		return vertex;
	}

	public Edge<Vertex> getEdge() {
		return edge;
	}

	public int getDistance() {
		return distance;
	}

	@Override
	public int compareTo(QueuedVertex<Vertex> it) {
		if (distance == it.distance)
			return 0;
		if (distance < it.distance)
			return -1;
		else
			return 1;
	}

}
