package tddt.graphs.shortestpath;

import com.google.common.base.Preconditions;
import tddt.graphs.Decider;
import tddt.graphs.DirectedGraph;
import tddt.graphs.Edge;

import java.util.*;

public class Dijkstra<Vertex> {

	private final DirectedGraph<Vertex> graph;
	private final Vertex fromVertex;

	public static <Vertex> Dijkstra<Vertex> getInstance(DirectedGraph<Vertex> graph, Vertex fromVertex) {
		return new Dijkstra<Vertex>(graph, fromVertex);
	}

	private Dijkstra(DirectedGraph<Vertex> graph, Vertex fromVertex) {
		Preconditions.checkArgument(graph.contains(fromVertex), "Graph does not contain initial vertex!");

		this.graph = graph;
		this.fromVertex = fromVertex;
	}

	public ResultPath<Vertex> findShortestPath(Decider<Vertex> decider) {
		if (decider.isFinal(fromVertex)) {
			return new ResultPath<Vertex>(Collections.singletonList(fromVertex), 0);
		}

		PriorityQueue<QueuedVertex<Vertex>> queue = new PriorityQueue<QueuedVertex<Vertex>>();
		Map<Vertex, Edge<Vertex>> predecessorTree = new HashMap<Vertex, Edge<Vertex>>();

		queue.add(new QueuedVertex<Vertex>(fromVertex, null, 0));

		while (!queue.isEmpty()) {
			QueuedVertex<Vertex> head = queue.poll();
			predecessorTree.put(head.getVertex(), head.getEdge());

			if (decider.isFinal(head.getVertex())) {
				List<Vertex> shortestPath = getPathInTree(fromVertex, head.getEdge().getTo(), predecessorTree);
				return new ResultPath<Vertex>(shortestPath, head.getDistance());
			}

			for (Edge<Vertex> edge : graph.getEdges(head.getVertex())) {
				if (predecessorTree.get(edge.getTo()) != null)
					continue;

				int toDistance = edge.getValue() + head.getDistance();

				queue.add(new QueuedVertex<Vertex>(edge.getTo(), edge, toDistance));
			}
		}

		return new ResultPath<Vertex>(Collections.<Vertex>emptyList(), 0);
	}

	private static <Vertex> List<Vertex> getPathInTree(Vertex root, Vertex leaf, Map<Vertex, Edge<Vertex>> predecessorTree) {
		ArrayList<Vertex> result = new ArrayList<Vertex>();
		Vertex currentVertex = leaf;
		result.add(currentVertex);

		while (currentVertex != null && !currentVertex.equals(root)) {
			currentVertex = predecessorTree.get(currentVertex).getFrom();
			result.add(currentVertex);
		}

		Collections.reverse(result);
		return result;
	}

}
