package tddt.graphs;

public class Edge<Vertex> {

	private final Vertex from;
	private final Vertex to;
	private final int value;

	public Edge(Vertex from, Vertex to, int value) {
		this.from = from;
		this.to = to;
		this.value = value;
	}

	public Vertex getFrom() {
		return from;
	}

	public Vertex getTo() {
		return to;
	}

	public int getValue() {
		return value;
	}

}
