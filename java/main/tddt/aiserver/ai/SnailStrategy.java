package tddt.aiserver.ai;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import tddt.aiserver.game.*;
import tddt.aiserver.gameutil.GameWalker;
import tddt.aiserver.gameutil.Warper;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class SnailStrategy implements MoveStrategy {

	private Map<Move, Move> MOVE_MAPPING = ImmutableMap.of(
			Move.UP_, Move.LEFT_,
			Move.LEFT_, Move.DOWN_,
			Move.DOWN_, Move.RIGHT_,
			Move.RIGHT_, Move.UP_);
	private Map<Move, Move> MOVE_REVERSE_MAPPING = ImmutableMap.of(Move.UP_, Move.RIGHT_, Move.RIGHT_, Move.DOWN_, Move.DOWN_, Move.LEFT_, Move.LEFT_, Move.UP_);

	private Move lastMove;

	public SnailStrategy() {
		this(Move.UP_);
	}

	public SnailStrategy(Move move) {
		this.lastMove = move;
	}

	@Override
	public Move makeMove(int aiPlayerId, GameState gameState) {
		List<Move> validMoves = getValidMoves(aiPlayerId, gameState, lastMove);
		List<Move> safeMoves = Lists.newArrayList();
		Map<Move, Integer> values = Maps.newHashMap();

		for (Move candidate : validMoves) {
			int regionSize = GameWalker.regionSize(gameState, getPointInDirectionFromHead(aiPlayerId, gameState, candidate));
			int numberOfBots = GameWalker.countBotsInRegion(gameState, getPointInDirectionFromHead(aiPlayerId, gameState, candidate), aiPlayerId);

			values.put(candidate, regionSize / (numberOfBots + 1));

			if (!isDangerMove(aiPlayerId, gameState, candidate)) {
				safeMoves.add(candidate);
			}
		}

		if (validMoves.isEmpty()) {
			return Move.DOWN_UP;
		}

		if (validMoves.size() == 1) {
			return doMove(validMoves.get(0));
		}

		if (safeMoves.isEmpty()) {
			return doMove(getBestMove(validMoves, values));
		}

		Move bestSafe = getBestMove(safeMoves, values);
		Move bestValid = getBestMove(validMoves, values);

		if (values.get(bestSafe) <= 5 && values.get(bestValid) > 10) {
			return doMove(bestValid);
		}

		return doMove(bestSafe);
	}

	private Move doMove(Move moveToDo) {
		lastMove = moveToDo;
		return moveToDo;
	}

	private Move getBestMove(List<Move> safeMoves, Map<Move, Integer> values) {
		Move moveToDo = safeMoves.get(0);

		for (Move move : safeMoves) {
			if (values.get(move) > values.get(moveToDo)) {
				moveToDo = move;
			}
		}
		return moveToDo;
	}

	private boolean isDangerMove(int aiPlayerId, GameState gameState, Move nextMove) {
		Set<Point> placesAround = Sets.newHashSet();

		for (Direction direction : Direction.values()) {
			placesAround.add(Warper.moveToDirection(gameState, getPointInDirectionFromHead(aiPlayerId, gameState, nextMove), direction));
		}

		Set<Player> livePlayers = gameState.getLivePlayers();

		Set<Point> playerHeads = Sets.newHashSet();

		for (Player player : livePlayers) {
			if (player.id == aiPlayerId) {
				continue;
			}

			playerHeads.add(gameState.getPlayerState(player).getHead());
		}

		return !Sets.intersection(placesAround, playerHeads).isEmpty();
	}

	private Point getPointInDirectionFromHead(int aiPlayerId, GameState gameState, Move nextMove) {
		return Warper.moveToDirection(gameState, getHead(aiPlayerId, gameState), nextMove.step1);
	}

	List<Move> getValidMoves(int aiPlayerId, GameState gameState, Move lastMove) {
		List<Move> validMoves = Lists.newArrayList();

		Move direction = MOVE_MAPPING.get(lastMove);
		addIfValid(validMoves, aiPlayerId, gameState, direction);

		for (int i = 0; i < 2; i++) {
			direction = MOVE_REVERSE_MAPPING.get(direction);
			addIfValid(validMoves, aiPlayerId, gameState, direction);
		}

		return validMoves;
	}

	private void addIfValid(List<Move> validMoves, int aiPlayerId, GameState gameState, Move direction) {
		if (isValidMove(aiPlayerId, gameState, direction)) {
			validMoves.add(direction);
		}
	}

	private boolean isValidMove(int aiPlayerId, GameState gameState, Move nextMove) {
		return gameState.isEmptyField(getPointInDirectionFromHead(aiPlayerId, gameState, nextMove));
	}

	private Point getHead(int aiPlayerId, GameState gameState) {
		return gameState.getPlayerState(aiPlayerId).getHead();
	}



}
