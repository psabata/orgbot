package tddt.aiserver.ai;

import tddt.aiserver.game.GameState;
import tddt.aiserver.game.Move;

public interface MoveStrategy {

	Move makeMove(int aiPlayerId, GameState gameState);

}
