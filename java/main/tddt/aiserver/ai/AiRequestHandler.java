package tddt.aiserver.ai;

import tddt.aiserver.serializers.SerializationUtil;
import tddt.aiserver.sockets.SimpleRequestHandler;

/**
 * Created by jpomikalek on 11/25/2015.
 */
public class AiRequestHandler implements SimpleRequestHandler
{
	private Ai ai;

	public AiRequestHandler(Ai ai)
	{
		this.ai = ai;
	}

	@Override
	public String processRequest(String request)
	{
		try
		{
			return processRequestInternal(request);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return e.toString();
		}
	}

	private String processRequestInternal(String request)
	{
		SerializationUtil.GameStateWithPlayerId gameStateWithPlayerId = SerializationUtil.deserializeGameStateWithPlayerId(request);
		return ai.makeMove(gameStateWithPlayerId.aiPlayerId, gameStateWithPlayerId.gameState).toString();
	}
}
