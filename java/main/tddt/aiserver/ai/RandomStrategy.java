package tddt.aiserver.ai;

import com.google.common.collect.Sets;
import tddt.aiserver.game.*;
import tddt.aiserver.gameutil.GameWalker;
import tddt.aiserver.gameutil.Warper;

import javax.annotation.Nullable;
import java.util.Random;
import java.util.Set;

public class RandomStrategy implements MoveStrategy {

	private final Random randomGenerator = new Random();

	@Override
	public Move makeMove(int myId, GameState gameState) {
		Move singleMove = getSingleStep(myId, gameState);

		return singleMove != null ? singleMove : Move.DOWN_;
	}

	@Nullable
	private Move getSingleStep(int myId, GameState gameState) {
		PlayerState myState = gameState.getPlayerState(myId);
		Point myHead = myState.getHead();

		Set<Move> maxRegionMoves = Sets.newHashSet();
		int maxRegionSize = 0;

		for (Direction direction : Direction.values()) {
			Point pointInDirection = Warper.moveToDirection(gameState, myHead, direction);
			if (gameState.isEmptyField(pointInDirection)) {
				Move candidateMove = new Move(direction);
				int regionSize = GameWalker.regionSize(gameState, pointInDirection);

				if (regionSize == maxRegionSize) {
					maxRegionMoves.add(candidateMove);
				} else if (regionSize > maxRegionSize) {
					maxRegionMoves = Sets.newHashSet(candidateMove);
					maxRegionSize = regionSize;
				}
			}
		}

		return randomElementFrom(maxRegionMoves);
	}

	@Nullable
	private <T> T randomElementFrom(Set<T> set) {
		if (set.isEmpty()) {
			return null;
		}

		int size = set.size();
		int item = randomGenerator.nextInt(size);
		int i = 0;

		for(T element : set) {
			if (i == item) {
				return element;
			}

			i++;
		}

		throw new IllegalStateException("No element was returned!");
	}

}
