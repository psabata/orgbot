package tddt.aiserver.ai;

import org.apache.commons.io.FileUtils;
import tddt.aiserver.game.GameState;
import tddt.aiserver.game.Move;

import java.io.*;
import java.util.Date;

public class ManyDeadlyAi implements Ai {

	private final MoveStrategy mainStrategy;
	private final MoveStrategy fallbackStrategy;

	public ManyDeadlyAi() {
		this(new SnailStrategy(), new RandomStrategy());
	}

	public ManyDeadlyAi(MoveStrategy mainStrategy, MoveStrategy fallbackStrategy) {
		this.mainStrategy = mainStrategy;
		this.fallbackStrategy = fallbackStrategy;
	}

	@Override
    public Move makeMove(int aiPlayerId, GameState gameState)
    {
        long startTime = System.currentTimeMillis();

        Move move;

        try {
            move = mainStrategy.makeMove(aiPlayerId, gameState);
        } catch (Exception exception) {
            logException(exception);
            move = fallbackStrategy.makeMove(aiPlayerId, gameState);
        }

        long timeTaken = System.currentTimeMillis() - startTime;

        System.out.println("timeTaken: " + timeTaken);

        if (timeTaken > 150)
        {
            throw new RuntimeException("It took too long: " + timeTaken);
        }

        return move;
    }

    private void logException(Exception exception) {
        PrintWriter printWriter = null;

        try
        {
            FileOutputStream fileOutputStream = FileUtils.openOutputStream(new File("../orgbot-log/error-log.txt"), true);
            printWriter = new PrintWriter(fileOutputStream);
            printWriter.println("\n - - - " + new Date() + " - - -\n");
            exception.printStackTrace(printWriter);
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            if (printWriter != null) {
				printWriter.close();
			}
        }
    }

}
