package tddt.aiserver.ai;

import tddt.aiserver.game.GameState;
import tddt.aiserver.game.Move;

/**
 * Created by jpomikalek on 11/9/2015.
 */
public interface Ai
{
	Move makeMove(int aiPlayerId, GameState gameState);
}
