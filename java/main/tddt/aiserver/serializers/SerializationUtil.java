package tddt.aiserver.serializers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import tddt.aiserver.game.GameState;
import tddt.aiserver.game.StandardGameState;

/**
 * Created by xpomikal on 28.11.15.
 */
public class SerializationUtil
{
	public static GameState deserializeGameState(String json)
	{
		return makeGameStateGsonDeserializer().fromJson(json, StandardGameState.class);
	}

	public static GameStateWithPlayerId deserializeGameStateWithPlayerId(String json)
	{
		return makeGameStateGsonDeserializer().fromJson(json, GameStateWithPlayerId.class);
	}

	private static Gson makeGameStateGsonDeserializer()
	{
		GsonBuilder gsonBuilder = new GsonBuilder();
		return gsonBuilder
				.enableComplexMapKeySerialization()
				.create();
	}

	public static class GameStateWithPlayerId
	{
		@Expose public final int aiPlayerId;
		@Expose public final StandardGameState gameState;

		public GameStateWithPlayerId(int aiPlayerId, GameState gameState)
		{
			this.aiPlayerId = aiPlayerId;
			this.gameState = new StandardGameState(gameState);
		}
	}
}
