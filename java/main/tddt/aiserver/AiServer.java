package tddt.aiserver;

import tddt.aiserver.ai.Ai;
import tddt.aiserver.ai.AiRequestHandler;
import tddt.aiserver.ai.ManyDeadlyAi;
import tddt.aiserver.game.GameState;
import tddt.aiserver.game.Move;
import tddt.aiserver.sockets.SimpleServer;

/**
 * Created by jpomikalek on 11/27/2015.
 */
public class AiServer
{
	public static final int PORT = 9005;

	public static void main(String[] args)
	{
		Ai ai = new ManyDeadlyAi();

		System.out.println("listening on port " + PORT);
		new SimpleServer(PORT, new AiRequestHandler(ai)).run();
	}
}
