package tddt.aiserver.game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by jpomikalek on 12/2/2015.
 */
public class GameStateCreator
{
	public static final char WALL = '*';
	public static final char EMPTY = ' ';

	public static GameState fromString(String gameStateAsString)
	{
		List<Player> players = new ArrayList<Player>();
		List<Point> startingPositions = new ArrayList<Point>();
		Set<Point> walls = new HashSet<Point>();

		String[] lines = gameStateAsString.replaceAll("\n$", "").split("\n");
		assertRectangularPlan(lines);

		int width = lines[0].length();
		int height = lines.length;

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				char ch = lines[y].charAt(x);

				if (ch == WALL)
					walls.add(new Point(x, y));
				else if (Character.isDigit(ch))
				{
					int playerId = Character.getNumericValue(ch);
					players.add(new Player(playerId, "Player " + playerId));
					startingPositions.add(new Point(x, y));
				}
				else if (ch != EMPTY)
					throw new GameStateCreationException("unrecognized character: " + ch);
			}
		}

		StandardGamePlan gamePlan = new StandardGamePlan(width, height, startingPositions, walls);
		return new StandardGameState(gamePlan, players);
	}

	private static void assertRectangularPlan(String[] lines)
	{
		int width = lines[0].length();

		for (int i = 1; i < lines.length; i++)
		{
			if (lines[i].length() != width)
			{
				throw new GameStateCreationException(
						String.format("non-rectangular plan: line %d width (%d) is different from the line 1 width (%d)",
									  (i + 1), lines[i].length(), width));
			}
		}
	}

	public static class GameStateCreationException extends RuntimeException
	{
		public GameStateCreationException(String message)
		{
			super(message);
		}
	}
}
