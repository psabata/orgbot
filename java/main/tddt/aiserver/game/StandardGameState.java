package tddt.aiserver.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.gson.annotations.Expose;

import java.util.*;

/**
 * Created by jpomikalek on 11/26/2015.
 */
public class StandardGameState implements GameState
{
	@Expose private StandardGamePlan gamePlan; // cannot be declared as GamePlan or GSON deserialization fails :(
	@Expose private List<Player> players;
	@Expose protected Map<Integer, StandardPlayerState> playerStateMap = new HashMap<Integer, StandardPlayerState>();
	@Expose protected Set<Player> livePlayers = new HashSet<Player>();
	private boolean[][] emptyFields = null;

	public StandardGameState(GamePlan gamePlan, Player... players)
	{
		this(gamePlan, Arrays.asList(players));
	}

	public StandardGameState(GamePlan gamePlan, List<Player> players)
	{
		this.gamePlan = new StandardGamePlan(gamePlan);
		this.players = ImmutableList.copyOf(players);

		if (players.size() != gamePlan.getStartingPositions().size())
		{
			throw new IllegalStateException(String.format("number of players (%d) is different from the number of starting positions (%d)",
					players.size(), gamePlan.getStartingPositions().size()));
		}

		for (int i = 0; i < players.size(); i++)
		{
			assertUniquePlayer(players.get(i));
			playerStateMap.put(players.get(i).id, new StandardPlayerState(gamePlan.getStartingPositions().get(i)));
			livePlayers.add(players.get(i));
		}
	}

	public StandardGameState(GameState gameState)
	{
		this.gamePlan = new StandardGamePlan(gameState.getGamePlan());
		this.players = ImmutableList.copyOf(gameState.getPlayers());
		this.livePlayers.addAll(gameState.getLivePlayers());

		for (Player player : players)
			playerStateMap.put(player.id, new StandardPlayerState(gameState.getPlayerState(player)));
	}

	@Override
	public boolean isGameOver()
	{
		return getLivePlayers().isEmpty();
	}

	@Override
	public PlayerState getPlayerState(Player player)
	{
		return playerStateMap.get(player.id);
	}

	@Override
	public PlayerState getPlayerState(int playerId)
	{
		return playerStateMap.get(playerId);
	}

	@Override
	public List<Player> getPlayers()
	{
		return players;
	}

	@Override
	public Set<Player> getLivePlayers()
	{
		return ImmutableSet.copyOf(livePlayers);
	}

	@Override
	public GamePlan getGamePlan()
	{
		return gamePlan;
	}

	@Override
	public boolean isEmptyField(Point position)
	{
		return isEmptyField(position.x, position.y);
	}

	@Override
	public boolean isEmptyField(int x, int y)
	{
		if (x < 0 || y < 0 || x >= gamePlan.getWidth() || y >= gamePlan.getHeight())
			return false;

		if (emptyFields == null)
			initializeEmptyFields();

		return emptyFields[x][y];
	}

	protected void invalidateEmptyFields()
	{
		emptyFields = null;
	}

	private void initializeEmptyFields()
	{
		emptyFields = new boolean[gamePlan.getWidth()][gamePlan.getHeight()];

		for (int x = 0; x < gamePlan.getWidth(); x++)
		{
			for (int y = 0; y < gamePlan.getHeight(); y++)
				emptyFields[x][y] = !gamePlan.isWall(new Point(x, y));
		}

		for (PlayerState playerState : playerStateMap.values())
		{
			for (Point segment : playerState.getAllSegments())
				emptyFields[segment.x][segment.y] = false;
		}
	}

	private void assertUniquePlayer(Player player)
	{
		if (livePlayers.contains(player))
			throw new IllegalStateException("duplicate player ID: " + player.id);
	}
}
