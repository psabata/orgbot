package tddt.aiserver.game;

import tddt.aiserver.ai.Ai;

import static tddt.aiserver.game.Direction.*;

/**
 * Created by jpomikalek on 12/2/2015.
 */
public class SampleAi implements Ai
{
	@Override
	public Move makeMove(int aiPlayerId, GameState gameState)
	{
		// Disclaimer: This is just an example.
		// Before you copy this code, be sure to check how exactly GameState#isEmptyField behaves.

		Point myPosition = gameState.getPlayerState(aiPlayerId).getHead();

		if (gameState.isEmptyField(LEFT.from(myPosition)))
			return Move.LEFT_;

		if (gameState.isEmptyField(RIGHT.from(myPosition)))
			return Move.RIGHT_;

		if (gameState.isEmptyField(UP.from(myPosition)))
			return Move.UP_;

		return Move.DOWN_;
	}
}
