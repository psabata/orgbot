package tddt.aiserver.game;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public class StandardPlayerState implements PlayerState
{
	@Expose private List<Point> segments;

	public StandardPlayerState(Point startingPosition)
	{
		segments = new ArrayList<Point>();
		segments.add(startingPosition);
	}

	public StandardPlayerState(PlayerState playerState)
	{
		segments = new ArrayList<Point>(Arrays.asList(playerState.getAllSegments()));
	}

	public void addSegment(Point position)
	{
		segments.add(position);
	}

	@Override
	public Point getHead()
	{
		return segments.get(segments.size() - 1);
	}

	/**
	 * Returns the coordinates of all segments. Tail first. Head last.
	 */
	@Override
	public Point[] getAllSegments()
	{
		return segments.toArray(new Point[segments.size()]);
	}
}
