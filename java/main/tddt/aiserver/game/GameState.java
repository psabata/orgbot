package tddt.aiserver.game;

import java.util.List;
import java.util.Set;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public interface GameState
{
	boolean isGameOver();

	PlayerState getPlayerState(Player player);

	PlayerState getPlayerState(int playerId);

	List<Player> getPlayers();

	Set<Player> getLivePlayers();

	GamePlan getGamePlan();

	/**
	 * @return true if the position is within the game plan boundaries and the field is not occupied by any player or a wall;
	 * false otherwise
	 */
	boolean isEmptyField(Point position);

	/**
	 * @return true if the [x, y] is within the game plan boundaries and the field is not occupied by any player or a wall;
	 * false otherwise
	 */
	boolean isEmptyField(int x, int y);
}
