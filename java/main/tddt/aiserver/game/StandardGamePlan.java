package tddt.aiserver.game;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.gson.annotations.Expose;

import java.util.List;
import java.util.Set;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public class StandardGamePlan implements GamePlan
{
	@Expose private int width;
	@Expose private int height;
	@Expose private List<Point> startingPositions;
	@Expose private Set<Point> walls;

	public StandardGamePlan(int width, int height, Iterable<Point> startingPositions)
	{
		this(width, height, startingPositions, ImmutableList.<Point>of());
	}

	public StandardGamePlan(int width, int height, Iterable<Point> startingPositions, Iterable<Point> walls)
	{
		this.width = width;
		this.height = height;
		this.startingPositions = ImmutableList.copyOf(startingPositions);
		this.walls = ImmutableSet.copyOf(walls);

		validateStartingPositions();
	}

	public StandardGamePlan(GamePlan gamePlan)
	{
		width = gamePlan.getWidth();
		height = gamePlan.getHeight();
		startingPositions = ImmutableList.copyOf(gamePlan.getStartingPositions());
		walls = ImmutableSet.copyOf(gamePlan.getWalls());
	}

	private void validateStartingPositions()
	{
		for (Point position : startingPositions)
		{
			if (isOutsideOfPlan(position))
				throw new IllegalStateException("starting position is outside of the plan: " + position);

			if (isWall(position))
				throw new IllegalStateException("starting position is on a wall: " + position);
		}
	}

	private boolean isOutsideOfPlan(Point position)
	{
		return position.x < 0 || position.y < 0 || position.x >= width || position.y >= height;
	}

	@Override
	public int getWidth()
	{
		return width;
	}

	@Override
	public int getHeight()
	{
		return height;
	}

	@Override
	public boolean isWall(Point where)
	{
		return walls.contains(where);
	}

	@Override
	public Set<Point> getWalls()
	{
		return walls;
	}

	@Override
	public List<Point> getStartingPositions()
	{
		return startingPositions;
	}
}
