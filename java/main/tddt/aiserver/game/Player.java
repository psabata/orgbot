package tddt.aiserver.game;

import com.google.gson.annotations.Expose;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public class Player implements Comparable<Player>
{
	@Expose public final int id;
	@Expose public final String name;

	public Player(int id, String name)
	{
		this.id = id;
		this.name = name;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Player player = (Player) o;

		return (id == player.id);
	}

	@Override
	public int hashCode()
	{
		return id;
	}

	@Override
	public String toString()
	{
		return "Player{" +
				"id=" + id +
				", name='" + name + '\'' +
				'}';
	}

	@Override
	public int compareTo(Player o)
	{
		return id - o.id;
	}
}
