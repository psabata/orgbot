package tddt.aiserver.game;

import javax.annotation.Nullable;

import static tddt.aiserver.game.Direction.*;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public class Move
{
	public static final Move UP_ = new Move(UP);
	public static final Move DOWN_ = new Move(DOWN);
	public static final Move LEFT_ = new Move(LEFT);
	public static final Move RIGHT_ = new Move(RIGHT);

	public static final Move UP_UP = new Move(UP, UP);
	public static final Move UP_DOWN = new Move(UP, DOWN);
	public static final Move UP_LEFT = new Move(UP, LEFT);
	public static final Move UP_RIGHT = new Move(UP, RIGHT);

	public static final Move DOWN_UP = new Move(DOWN, UP);
	public static final Move DOWN_DOWN = new Move(DOWN, DOWN);
	public static final Move DOWN_LEFT = new Move(DOWN, LEFT);
	public static final Move DOWN_RIGHT = new Move(DOWN, RIGHT);

	public static final Move LEFT_UP = new Move(LEFT, UP);
	public static final Move LEFT_DOWN = new Move(LEFT, DOWN);
	public static final Move LEFT_LEFT = new Move(LEFT, LEFT);
	public static final Move LEFT_RIGHT = new Move(LEFT, RIGHT);

	public static final Move RIGHT_UP = new Move(RIGHT, UP);
	public static final Move RIGHT_DOWN = new Move(RIGHT, DOWN);
	public static final Move RIGHT_LEFT = new Move(RIGHT, LEFT);
	public static final Move RIGHT_RIGHT = new Move(RIGHT, RIGHT);

	public final Direction step1;
	public final Direction step2;

	public Move(Direction step1)
	{
		this(step1, null);
	}

	public Move(Direction step1, @Nullable Direction step2)
	{
		if (step1 == null)
			throw new IllegalArgumentException("step1 must not be null");

		this.step1 = step1;
		this.step2 = step2;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Move move = (Move) o;

		return (step1 == move.step1 && step2 == move.step2);
	}

	@Override
	public int hashCode()
	{
		int result = step1.hashCode();
		result = 31 * result + (step2 != null ? step2.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		if (step2 == null)
			return step1.toString();
		else
			return step1.toString() + step2.toString();
	}
}
