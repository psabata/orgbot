package tddt.aiserver.game;

import java.util.List;
import java.util.Set;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public interface GamePlan
{
	int getWidth();

	int getHeight();

	boolean isWall(Point where);

	Set<Point> getWalls();

	List<Point> getStartingPositions();
}
