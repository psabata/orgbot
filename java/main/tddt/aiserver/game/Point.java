package tddt.aiserver.game;

import com.google.gson.annotations.Expose;

/**
* Created by jpomikalek on 11/4/2015.
*/
public class Point
{
	@Expose public final int x;
	@Expose public final int y;

	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Point point = (Point) o;

		return (x == point.x && y == point.y);
	}

	@Override
	public int hashCode()
	{
		int result = x;
		result = 31 * result + y;
		return result;
	}

	@Override
	public String toString()
	{
		return "Point{" +
				"x=" + x +
				", y=" + y +
				'}';
	}
}
