package tddt.aiserver.game;

/**
 * Created by jpomikalek on 11/4/2015.
 */
public interface PlayerState
{
	Point getHead();

	Point[] getAllSegments();
}
