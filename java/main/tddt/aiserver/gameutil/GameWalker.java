package tddt.aiserver.gameutil;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import tddt.aiserver.game.*;

import java.util.Queue;
import java.util.Set;

public class GameWalker {

    public static int regionSize(GameState gameState, Point startingPoint) {
        if (!gameState.isEmptyField(startingPoint)) {
            return 0;
        }

        Set<Point> visited = Sets.newHashSet();
        Queue<Point> queue = Lists.newLinkedList();

        visitAndAddToQueue(queue, visited, startingPoint);

        while (!queue.isEmpty()) {
            Point head = queue.remove();

            for (Direction direction : Direction.values()) {
                Point candidatePoint = Warper.moveToDirection(gameState, head, direction);

                if (isValidAndNotVisited(gameState, visited, candidatePoint)) {
                    visitAndAddToQueue(queue, visited, candidatePoint);
                }
            }
        }

        return visited.size();
    }

    private static void visitAndAddToQueue(Queue<Point> queue, Set<Point> visited, Point startingPoint) {
        visited.add(startingPoint);
        queue.add(startingPoint);
    }

    private static boolean isValidAndNotVisited(GameState gameState, Set<Point> visited, Point candidatePoint) {
        return !visited.contains(candidatePoint) && gameState.isEmptyField(candidatePoint);
    }

    public static int countBotsInRegion(GameState gameState, Point startingPoint, int myId) {
        if (!gameState.isEmptyField(startingPoint)) {
            return 0;
        }

        Set<Point> visited = Sets.newHashSet();
        Queue<Point> queue = Lists.newLinkedList();

        Set<Point> livePlayerPoints = getAllLivePlayersPoints(gameState, myId);
        Set<Point> livePlayersInRegion = Sets.newHashSet();

        visitAndAddToQueue(queue, visited, startingPoint);

        while (!queue.isEmpty()) {
            Point head = queue.remove();

            for (Direction direction : Direction.values()) {
                Point candidatePoint = Warper.moveToDirection(gameState, head, direction);
                if (livePlayerPoints.contains(candidatePoint)) {
                    livePlayersInRegion.add(candidatePoint);
                } else if (isValidAndNotVisited(gameState, visited, candidatePoint)) {
                    visitAndAddToQueue(queue, visited, candidatePoint);
                }
            }
        }

        return livePlayersInRegion.size();
    }

    private static Set<Point> getAllLivePlayersPoints(GameState gameState, int myId) {
        Set<Point> livePlayerPoints = Sets.newHashSet();

        for (Player player : gameState.getLivePlayers()) {
            PlayerState playerState = gameState.getPlayerState(player);

            if (player.id != myId) {
                Point playerHead = playerState.getHead();
                livePlayerPoints.add(playerHead);
            }
        }

        return livePlayerPoints;
    }
}
