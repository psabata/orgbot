package tddt.aiserver.gameutil;

import tddt.aiserver.game.Direction;
import tddt.aiserver.game.GameState;
import tddt.aiserver.game.Point;

public class Warper {

    public static Point moveToDirection(GameState gameState, Point myHead, Direction direction) {
        return normalize(gameState, direction.from(myHead));
    }

    private static Point normalize(GameState gameState, Point point) {

        int planWidth = gameState.getGamePlan().getWidth();
        int planHeight = gameState.getGamePlan().getHeight();

        int x = normalizeCoordinate(point.x, planWidth);
        int y = normalizeCoordinate(point.y, planHeight);

        return new Point(x, y);
    }

    private static int normalizeCoordinate(int coordinate, int borderLimit) {
        if (coordinate < 0) {
            coordinate = borderLimit + coordinate;
        } else if (coordinate >= borderLimit) {
            coordinate = coordinate - borderLimit;
        }
        return coordinate;
    }

}
