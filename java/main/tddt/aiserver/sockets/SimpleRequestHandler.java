package tddt.aiserver.sockets;

/**
 * Created by jpomikalek on 11/13/2015.
 */
public interface SimpleRequestHandler
{
	String processRequest(String request);
}
