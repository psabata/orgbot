#!/usr/bin/perl
# To run  with CRON, enter
# sudo crontab -e
# Then edit the file to have something like to run every 29th and 59th minute of the hour:
# 29,59 * * * * su -s /bin/sh nsbuild -c '/opt/NetLedger_Impact/tdd/bjorns-tddt-aiserverstub/auto-update.pl > /opt/NetLedger_Impact/tdd/log/cron-log.txt'
#
# For testing purposes, you can set it to run every minute using this:
# */1 * * * * /opt/NetLedger_Impact/tdd/tddt-aiserverstub-bjorn/auto-update.pl > /opt/NetLedger_Impact/tdd/log/cron-log.txt
#
# To track the output of this script, go to the log directory and type:
# tail -F -n60 cron-log.txt
#
# To track the server output, go to the log directory and type:
# tail -F -n60 cron-log.txt

use strict;
use warnings;

my $botGitDir = '/home/nsbuild/orgbot/';
my $serverLog = '/home/nsbuild/orgbot-log/server-log.txt';

chdir $botGitDir;
print "Local git repository path set to: $botGitDir.\n";

# kill the server if it's running
&announce("Stopping server if it is running");
&executeAndEcho("./stop.exp");

# pull from git
&announce("Pulling from git");
&executeAndEcho("git pull");

# build the code
&announce("Building");
&executeAndEcho("./build.sh");

# start the new server
&announce("Starting server");
&executeAndEcho("./run.sh > $serverLog &");

print "Server started.\n";


sub announce {
	my $message = shift;
	print "\n\n********** ". localtime, " $message **********\n";
}

sub executeAndEcho {
	my $command = shift;
	my $output = "Executing $command\n";
	$output.=`$command 2>&1`;
	if (${^CHILD_ERROR_NATIVE}) {
		$output.= "----- ERROR WHEN EXECUTING $command -----\n";
		$output.= "Exit code ${^CHILD_ERROR_NATIVE}\n";
	}
	print $output, "\n";
}
