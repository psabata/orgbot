#!/bin/sh

JAVAC=/opt/jdk1.6/bin/javac

CLASSPATH="$CLASSPATH:java/lib/*"

CLASSPATH="$CLASSPATH:java/main"
CLASSPATH="$CLASSPATH:java/test"

export CLASSPATH

find java/ -name '*.java' | xargs $JAVAC
