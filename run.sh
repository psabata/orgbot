#!/bin/sh

JAVA=/opt/jdk1.6/bin/java

CLASSPATH="$CLASSPATH:java/lib/google-collections-0.9.jar"
CLASSPATH="$CLASSPATH:java/lib/gson-2.4.jar"
CLASSPATH="$CLASSPATH:java/lib/hamcrest-all-1.0.jar"
CLASSPATH="$CLASSPATH:java/lib/jsr305-1.3.9.jar"
CLASSPATH="$CLASSPATH:java/lib/junit-4.7.jar"

CLASSPATH="$CLASSPATH:java/main"
CLASSPATH="$CLASSPATH:java/test"

export CLASSPATH

$JAVA tddt.aiserver.AiServer
